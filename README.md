# README #

Proyecto de RED DE BICICLETAS, curso NODE.js.

### What is this repository for? ###

* Proyecto realizado en NODE.js paginas front y back API, Opciones de consulta, elimación, agregar y actualización de nuevas bicicletas.
* 1.0
* [Learn Markdown](https://javierzuluaga20@bitbucket.org/javierzuluaga20/red-bicicletas.git)

### How do I get set up? ###

* Summary of set up
* Configuration

* Dependencies
    cookie-parser
    debug
    express
    http-errors
    morgan
    pug
    nodemon

* Database configuration
No tiene base de datos asociada, la información se maneja en memoria.

* How to run tests
Ingresar a las siguientes URL desde una navegador web.

Sitio WEB
http://localhost:3000/

API
http://localhost:3000/api/bicicletas


* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact