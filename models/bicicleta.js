var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: '2dsphere', sparce: true}
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + ' | color: ' + this.color;
}

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({},cb);
}

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb); //create viene de la mongoose
};

bicicletaSchema.statics.findByCode = function(aCode, cb){
    this.findOne({code: aCode}, cb); //findOne viene de la mongoose
};

bicicletaSchema.statics.removeByCode = function(aCode, cb){
    this.deleteOne({code: aCode}, cb); //deleteOne viene de la mongoose
};



module.exports = mongoose.model('Bicicleta', bicicletaSchema);


/*
//ANTES DE INCLUIR LA PERSISTENCIA DE DB


var Bicicleta = function (id, color, modelo, ubicacion ){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;

}

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + " color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);

}


Bicicleta.removeById = function(aBiciId){
    var aBici = Bicicleta.findById(aBiciId);
    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.slice(i,1);
            break;
        }
    }


}
*/

/*
var a = new Bicicleta(1, 'rojo', 'urbana', [45.5042441,-73.6251757]);
var b = new Bicicleta(2, 'blanca', 'urbana',[45.5060487,-73.6275361]);

Bicicleta.add(a);
Bicicleta.add(b);
*/

//module.exports = Bicicleta;