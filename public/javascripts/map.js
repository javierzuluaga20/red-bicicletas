var map = L.map('main_map').setView([45.4961742,-73.6132167], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiamF2aWVyenVsdWFnYSIsImEiOiJja2NtOHVlOWowMHJ3MnNwcWMwODV1bWM2In0.WzjZZXs3FTAzPK3unpgtgw'
}).addTo(map);

/*
L.marker([45.4846302,-73.616936]).addTo(map);
L.marker([45.5034621,-73.6226008]).addTo(map);
L.marker([45.5032816,-73.6155627]).addTo(map);
*/


$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
        });

    }

})

