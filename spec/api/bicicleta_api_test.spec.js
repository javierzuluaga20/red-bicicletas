var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:3000/api/bicicletas";


describe ('Bicicletas API', () => {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true});

        const db = mongoose.connection;

        db.on('error', console.error.bind(console, 'conection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });

    });
    
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });


    describe('GET BICICLETAS /',() => {
        it('Status 200',(done) => {           
           request.get(base_url, function(error, response, body){
               var result = JSON.parse(body);
               //console.log('salida result: '+ result);
               expect(response.statusCode).toBe(200);
               //console.log('salida resultado: ' + result);
               //expect(result.bicicletas).toBeUndefined();
               expect(result.bicis.length).toBe(0);               
               done();
            });
        });
    });

    describe('POST BICICLETAS /create',() => {
        it('Status 200',(done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id" : 10, "color" : "rojo", "modelo":"urbana", "lat" :-34, "lng" :-54 }';
            request.post({
                headers : headers,
                url : base_url + '/create',
                body : aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });

        });
    });

});

/*
describe('Bicicletas API', () => {
    describe('GET BICICLETAS /',() => {
        it('Status 200',() => {
            Bicicleta.allBicis = [];
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(10, 'negro', 'urbana', [45.5042441,-73.6251757]);
            Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });


    describe('POST BICICLETAS /create',() => {
        it('Status 200',(done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id" : 10, "color" : "rojo", "modelo":"urbana", "lat" :-34, "lng" :-54 }';
            request.post({
                headers : headers,
                url : 'http://localhost:5000/api/bicicletas/create',
                body : aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });

        });
    });
    */
    /*
    describe('POST BICICLETAS /update',() => {
        it('Status 200',(done) => {
            var headers = {'Content-Type' : 'application/json'};
            var aBici = '{"id" : 10, "color" : "amarilla", "modelo" : "urbana", "lat" :45.4983541, "lng" :-73.6189926 }';
            request.post({
                headers : headers,
                url : 'http://localhost:5000/api/bicicletas/update',
                body : aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("amarilla");
                done();
            });

        });
    });

*/

//});